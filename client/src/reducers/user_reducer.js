import { FETCH_USER, ERROR_RESPONSE, FETCH_USERS } from '../actions/types';

const INITIAL_STATE = { profile: {}, message: '', error: '' };

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_USER:
    console.log(action)
      return { ...state, profile: action.payload };
    case ERROR_RESPONSE:
      return { ...state, error: action.payload };
    //nowe
    case FETCH_USERS:
      console.log(action)

      return { ...state, users: action.payload};
      //return { ...state, usersList: {users: action.payload};
      }

  return state;
}
