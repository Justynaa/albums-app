import {  AUTH_ALBUM, FETCH_ALBUMS, ERROR_RESPONSE } from '../actions/types';

const INITIAL_STATE = { profile: {}, message: '', error: '' };

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case AUTH_ALBUM:
      return { ...state, error: '', message: '', authenticated: true };
    case FETCH_ALBUMS:
    console.log(action)
      return { ...state, profile: action.payload };
    case ERROR_RESPONSE:
      return { ...state, error: action.payload };
  }

  return state;
}
