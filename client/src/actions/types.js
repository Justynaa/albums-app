export const AUTH_USER = 'auth_user',
  UNAUTH_USER = 'unauth_user',
  AUTH_ERROR = 'auth_error';

export const FETCH_USER = 'fetch_user';
export const FETCH_USERS = 'fetch_users';

export const FETCH_ALBUMS = 'fetch_album';
export const AUTH_ALBUM = 'auth_album';