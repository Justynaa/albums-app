﻿import axios from 'axios';
import { browserHistory } from 'react-router';
import cookie from 'react-cookie';
import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import { AUTH_USER, AUTH_ERROR, UNAUTH_USER, PROTECTED_TEST, FETCH_USER, FETCH_USERS, AUTH_ALBUMS, FETCH_ALBUMS } from './types';


export function loginUser({ email, password }) {
  return function (dispatch) {
    axios.post(`${API_URL}/auth/login`, { email, password })
    .then((response) => {
      cookie.save('token', response.data.token, { path: '/' });
      cookie.save('user', response.data.user, { path: '/' });
      dispatch({ type: AUTH_USER });
      window.location.href = `${CLIENT_ROOT_URL}/dashboard`;
    })
    .catch((error) => {
      errorHandler(dispatch, error.response, AUTH_ERROR);
    });
  };
}

export function registerUser({ email, firstName, lastName, password }) {
  return function (dispatch) {
    axios.post(`${API_URL}/auth/register`, { email, firstName, lastName, password})
    .then((response) => {
      console.log(response)
      cookie.save('token', response.data.token, { path: '/' });
      cookie.save('user', response.data.user, { path: '/' });
      dispatch({ type: AUTH_USER });
      window.location.href = `${CLIENT_ROOT_URL}/dashboard`;
    })
    .catch((error) => {
      errorHandler(dispatch, error.response, AUTH_ERROR);
    });
  };
}

export function logoutUser(error) {
  return function (dispatch) {
    dispatch({ type: UNAUTH_USER });
    cookie.remove('token', { path: '/' });
    cookie.remove('user', { path: '/' });

    window.location.href = `${CLIENT_ROOT_URL}/login`;
  };
}


export function protectedTest() {
  return function (dispatch) {
    axios.get(`${API_URL}/protected`, {
      headers: { Authorization: cookie.load('token') },
    })
    .then((response) => {
      dispatch({
        type: PROTECTED_TEST,
        payload: response.data.content,
      });
    })
    .catch((error) => {
      errorHandler(dispatch, error.response, AUTH_ERROR);
    });
  };
}

export function fetchUser(uid) {
  return function (dispatch) {
    axios.get(`${API_URL}/user/${uid}`, {
      headers: { Authorization: cookie.load('token') },
    })
    .then((response) => {
      dispatch({
        type: FETCH_USER,
        payload: response.data.user,
      });
    })
    .catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchUsers() {
  return function (dispatch) {
    axios.get(`${API_URL}/users`, {
      headers: { Authorization: cookie.load('token') },
    })
        .then((response) => {
          dispatch({
            type: FETCH_USERS,
            payload: response.data.user,
          });
window.location.href = `${CLIENT_ROOT_URL}/allusers`;
        })
        .catch(response => dispatch(errorHandler(response.data.error)));

  };
}

//ALBUMY
export function registerAlbums({ title, artist, cover}) {
  return function (dispatch) {
    axios.post(`${API_URL}/auth/registerAlbum`, { title, artist, cover})
        .then((response) => {
          console.log(response)
         // cookie.save('album', response.data.album, { path: '/' });
          dispatch({ type: AUTH_ALBUM });
        })
        .catch((error) => {
          errorHandler(dispatch, error.response, AUTH_ERROR);
        });
  };
}


export function fetchAlbums() {
  return function (dispatch) {
    axios.get(`${API_URL}/album/allalbums`)
        .then((response) => {
          dispatch({
            type: FETCH_ALBUMS,
            payload: response.data.albums,
          });
window.location.href = `${CLIENT_ROOT_URL}/allalbums`;
        })
        .catch(response => dispatch(errorHandler(response.data.error)));
  };
}