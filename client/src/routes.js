import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import NotFoundPage from './components/pages/not-found-page';
import HomePage from './components/pages/home-page';
import Register from './components/auth/register';
import Login from './components/auth/login';
import Logout from './components/auth/logout';
import Dashboard from './components/dashboard/dashboard';
import AlbumsPage from './components/dashboard/albums-page';
import UsersList from './components/dashboard/users-list';
import RequireAuth from './components/auth/require_auth';
import AlbumRegister from './components/auth/registerAlbum';

export default (
	<Route path="/" component={App}>
		<IndexRoute component={HomePage} />
		<Route path="/register" component={Register} />
		<Route path="/login" component={Login} />
		<Route path="/logout" component={Logout} />
		<Route path="/dashboard" component={RequireAuth(Dashboard)} />
		<Route path="/allalbums" component={AlbumsPage} />
		<Route path="/registerAlbum" component={AlbumRegister} />
		<Route path="/allusers" component={UsersList} />
		<Route path="*" component={NotFoundPage} />

	</Route>
)