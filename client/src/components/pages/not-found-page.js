import React, { Component } from 'react';

class NotFoundPage extends Component {

  render() {
    return (
      <div>
        <h1>404</h1>
        <p>Strona nie została odnaleziona.</p>
      </div>
    )
  }
}
export default NotFoundPage;  