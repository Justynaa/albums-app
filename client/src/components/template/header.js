import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class HeaderTemplate extends Component {
  renderLinks() {
    if (this.props.authenticated) {
      return [
        <li className="nav-item">
          <Link className="nav-link" to="/">Strona główna</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="dashboard">Dashboard</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="/allalbums">Albumy</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="/registeralbum">Dodaj nowy</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="/allusers">Użytkownicy</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="logout">Wyloguj</Link>
        </li>,
      ];
    } else {
      return [
        // Unauthenticated navigation
        <li className="nav-item">
          <Link className="nav-link" to="/">Strona główna</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="login">Zaloguj</Link>
        </li>,
        <li className="nav-item">
          <Link className="nav-link" to="register">Zarejestruj</Link>
        </li>,
      ];
    }
  }

  render() {
    return (
          <div className="menu_container">
            <div className="menu_navigation">
              <ul className="nav flex-column">
                {this.renderLinks()}
              </ul>
            </div>
          </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
  };
}

export default connect(mapStateToProps)(HeaderTemplate);
