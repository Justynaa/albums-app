﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { registerAlbums } from '../../actions/auth';

const form = reduxForm({
  form: 'registerAlbums',
  validate,
});

const renderField = field => (
  <div>
    <input className="form-control" {...field.input} />
    {field.touched && field.error && <div className="error">{field.error}</div>}
  </div>
);

function validate(formProps) {
  const errors = {};

  if (!formProps.title) {
    errors.title = 'Podaj tytuł albumu.';
  }

  if (!formProps.artist) {
    errors.artist = 'Podaj autora.';
  }

  if (!formProps.cover) {
    errors.cover = 'Podaj okładkę.';
  }


  return errors;
}

class RegisterAlbum extends Component {
  handleFormSubmit(formProps) {
    this.props.registerAlbums(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <span><strong>Błąd!</strong> {this.props.errorMessage}</span>
        </div>
      );
    }
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        {this.renderAlert()}
        <div className="row">
          <div className="col-md-6">
            <label>Tytuł</label>
            <Field name="title" className="form-control" component={renderField} type="text" />
          </div>
          <div className="col-md-6">
            <label>Autor</label>
            <Field name="artist" className="form-control" component={renderField} type="text" />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <label>Okładka</label>
            <Field name="cover" className="form-control" component={renderField} type="text" />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">Register</button>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    authenticated: state.auth.authenticated,
  };
}

export default connect(mapStateToProps, { registerAlbums })(form(RegisterAlbum));
