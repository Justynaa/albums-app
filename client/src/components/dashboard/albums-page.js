import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import { fetchUser } from '../../actions/auth';
import { fetchAlbums } from '../../actions/auth';
import Profile from './profile';
import UserPanel from './user-panel';
import AlbumsList from './albums-list';

class AlbumsPage extends Component {

    constructor(props) {
        super(props);
        console.log(this.props)
    }

    componentDidMount(){
        this.props.fetchAlbums();
        console.log(this.props.albums);
    }

    memberMenu() {
        return (
            <div className="client-menu">
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.memberMenu()}
                <p>{this.props.content}</p>
            </div>
        );
    }
}

AlbumsPage.propTypes = {
    albums: React.PropTypes.array.isRequired,
    fetchAlbums: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        albums: state.album
    }
}
export default connect(mapStateToProps, { fetchAlbums})(AlbumsPage);
