import React, { Component } from 'react';

class Profile extends Component {
  render() {
    return (
        <div>
          Zalogowany jako: {this.props.profile.firstName} {this.props.profile.lastName} {this.props.profile.email}
        </div>

    );
  }
}

export default Profile;
