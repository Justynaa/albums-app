/**
 * Created by Justyna on 2017-01-26.
 */
import React, { Component } from 'react';
import { Link, Match } from 'react-router';
import AlbumsPage from './albums-page';

class UserPanel extends Component {
    renderLinks() {
            return [
                <li className="nav-item">
                    <Link className="nav-link" to="albums">Albumy</Link>
                </li>,
                <li className="nav-item">
                    <Link className="nav-link" to="">Dodaj nowy</Link>
                </li>,
            ];

    }
    render(){
        return(
            <div className="nav">

                    <ul className="nav justify-content-center">
                        {this.renderLinks()}
                    </ul>

            </div>

        )
    }

}

export default UserPanel;