  import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import { protectedTest, fetchUser, fetchUsers } from '../../actions/auth';
import Profile from './profile';
  import UserPanel from './user-panel';


class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.props.protectedTest();
    console.log(this.props);
  }

  componentDidMount(){
    const userId = cookie.load('user');
    this.props.fetchUser(userId._id);
    //console.log('uzytkownicy');
    //console.log(this.props.fetchUsers());
    //this.props.fetchUsers();
  }

  memberMenu() {
    return (
      <div className="client-menu">
        <Profile profile={this.props.profile}/>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.memberMenu()}
        <p>{this.props.content}</p>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { content: state.auth.content, authenticated: state.auth.authenticated, profile: state.user.profile };
}

export default connect(mapStateToProps, { protectedTest, fetchUser})(Dashboard);
