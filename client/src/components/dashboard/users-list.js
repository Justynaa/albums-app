import React, {Component} from 'react';
import {fetchUsers, protectedTest} from '../../actions/auth';
import { connect } from 'react-redux';

class UsersList extends Component{

    constructor(props) {
        super(props);
        this.props.protectedTest();
        console.log(this.props);
    }

    componentDidMount() {
        this.props.fetchUsers();
    }

    render() {
        console.log("displaying");
        //console.log(self.state);
        var table ;
        if (this.props.users instanceof Array) {
            table = this.props.users.map((row) =>
                <tr>
                    <td>{row.firstName}</td>
                    <td>{row.lastName}</td>
                </tr>
            );
        }

        console.log("table");
        console.log(table);
        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                </tr>
                </thead>
                <tbody>
                {table}
                </tbody>
            </table>
        );
    }
}

function mapStateToProps(state) {
    return { profile: state.user.profile };
}

export default connect(mapStateToProps, {protectedTest, fetchUsers})(UsersList);