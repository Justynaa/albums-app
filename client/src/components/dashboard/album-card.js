/**
 * Created by Justyna on 2017-01-27.
 */
import React from 'react';

export default function AlbumCard({ album }) {
    return (
        <div className="ui card">
            <div className="image">
                <img src={album.cover} alt="Album Cover" />
            </div>
            <div className="content">
                <div className="header">{album.title}</div>
            </div>
        </div>
    );
}

AlbumCard.propTypes = {
    album: React.PropTypes.object.isRequired
}