import React, {Component} from 'react';
import {fetchAlbums, protectedTest} from '../../actions/auth';
import { connect } from 'react-redux';

class AlbumsList extends Component{

    constructor(props) {
        super(props);
        this.props.protectedTest();
        console.log(this.props);
    }

    componentDidMount() {
        this.props.fetchAlbums();
    }
    
    render() {
    	console.log("displaying");
        var table;
        if (this.props.albums instanceof Array) {
            table = this.props.albums.map((row) =>
                <tr>
                    <td>{row.title}</td>
                    <td>{row.artist}</td>
                </tr>
            );
        }

        console.log("table");
        console.log(table);
        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Tytuł</th>
                    <th>Wykonawca</th>
                </tr>
                </thead>
                <tbody>
                {table}
                </tbody>
            </table>
        ); 
    }    
}

function mapStateToProps(state) {
    return { album: state.albums };
}
AlbumsList.propTypes = {
    Album: React.PropTypes.array.isRequired
}

export default connect(mapStateToProps, {protectedTest, fetchAlbums})(AlbumsList);