var mongoose = require('mongoose');
const User = require('../models/user');
const setUserInfo = require('../helpers').setUserInfo;

exports.viewProfile = function (req, res, next) {
  const userId = req.params.userId;

  if (req.user._id.toString() !== userId) { return res.status(401).json({ error: 'Nie mo�esz zobaczy� tego profilu.' }); }
  User.findById(userId, (err, user) => {
    if (err) {
      res.status(400).json({ error: 'Nie ma u�ytkownika o takim ID.' });
      return next(err);
    }

    const userToReturn = setUserInfo(user);

    return res.status(200).json({ user: userToReturn });
  });
};

exports.getUsers = function(res) {
  var twisted = function (res) {
    return function (err, data) {
      if (err) {
        console.log('error occured');
        return res.status(200).json({ std:"Hello world 2" });
      }
      res.send('U�ytkownicy:\n');
      console.log(data);
      return res.status(200).json({ std:"Hello world 2" });
    }
  }

  User.find({}, 'firstName', twisted(res));
}
