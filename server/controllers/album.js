var mongoose = require('mongoose');
var Album = mongoose.model('Album');
const setAlbumInfo = require('../helpers').setAlbumInfo;

exports.albumlist = function albumlist(req, res, next) { 
  Album.find({}, (err, album) => {
    if (err) {
      console.log(err); return next(err);
    }

      console.log(album);
      const albumsToReturn = setAlbumInfo(album);
      res.status(200).json({album: albumsToReturn});
  }); 
};