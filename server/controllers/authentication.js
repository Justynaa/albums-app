﻿const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const User = require('../models/user');
const setUserInfo = require('../helpers').setUserInfo;
const setAlbumInfo = require('../helpers').setAlbumInfo;
const config = require('../config/main');

const Album = require('../models/Album');

function generateToken(user) {
  return jwt.sign(user, config.secret, {
    expiresIn: 604800
  });
}

exports.login = function (req, res, next) {
  const userInfo = setUserInfo(req.user);

  res.status(200).json({
    token: `JWT ${generateToken(userInfo)}`,
    user: userInfo
  });
};


exports.register = function(req, res, next) {
  const email = req.body.email;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const password = req.body.password;

  if (!email) {
    return res.status(422).send({ error: 'Musisz podać e-mail.'});
  }

  if (!firstName || !lastName) {
    return res.status(422).send({ error: 'Musisz podać imię i nazwisko.'});
  }

  if (!password) {
    return res.status(422).send({ error: 'Musisz podać hasło.' });
  }


  User.findOne({ email: email }, function(err, existingUser) {
    if (err) { return next(err); }

    if (existingUser) {
      return res.status(422).send({ error: 'Użytkownik z tym e-mailem już istnieje.' });
    }

    let user = new User({
      email: email,
      password: password,
      profile: { firstName: firstName, lastName: lastName }
    });

    user.save(function(err, user) {
      if (err) { return next(err); }

      let userInfo = setUserInfo(user);

      res.status(201).json({
        token: `JWT ${generateToken(userInfo)}`,
        user: userInfo
      });
    });
  });
}

//ALBUMY
exports.registerAlbum = function(req, res, next) {
  // Check for registration errors
  const title = req.body.title;
  const artist = req.body.artist;
  const cover = req.body.cover;

  if (!title) {
    return res.status(422).send({ error: 'Musisz podać tytuł albumu.'});
  }

  if (!artist) {
    return res.status(422).send({ error: 'Musisz podać autora albumu.'});
  }

  if (!cover) {
    return res.status(422).send({ error: 'Musisz podać okładkę albumu.' });
  }

  let album = new Album({
    title: title,
    artist: artist,
    cover: cover
  });

  album.save(function(err, album) {
    if (err) { return next(err); }

    let albumInfo = setAlbumInfo(album);

    res.status(201).json({
      token: `JWT ${generateToken(albumInfo)}`,
      album: albumInfo
    });
  });
};


