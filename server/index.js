const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      mongoose = require('mongoose'),
      logger = require('morgan'),
      router = require('./router');
      config = require('./config/main');

mongoose.connect(config.database);
mongoose.connection.on('error', function() {
	  console.info('ERROR: Cannot establish connection with MongoDB.');
	});

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT,GET,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
})

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(logger('dev'));

const server = app.listen(config.port);
console.log('Your server is running on '+ config.port + '.')

router(app);
