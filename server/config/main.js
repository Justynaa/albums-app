module.exports = {
  'secret': 'super secret passphrase',
  'database': 'mongodb://mongodb://localhost:27017/albums-proj',
  'port': process.env.PORT || 3000
}