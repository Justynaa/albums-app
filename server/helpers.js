exports.setUserInfo = function setUserInfo(request) {
  const getUserInfo = {
    _id: request._id,
    firstName: request.profile.firstName,
    lastName: request.profile.lastName,
    email: request.email

  };

  return getUserInfo;
};

exports.setAlbumInfo = function setAlbumInfo(request) {
  const getAlbumInfo = {
    _id: request._id,
    title: request.title,
    artist: request.artist,
    cover: request.cover

  };

  return getAlbumInfo;
};