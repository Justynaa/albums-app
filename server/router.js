const AuthenticationController = require('./controllers/authentication');
const UserController = require('./controllers/user');
const AlbumController = require('./controllers/album');
const express = require('express');
const passport = require('passport');

const passportService = require('./config/passport');
const requireAuth = passport.authenticate('jwt',{ session: false });
const requireLogin = passport.authenticate('local',{ session: false });

module.exports = function(app) {  
  const apiRoutes = express.Router(),
    authRoutes = express.Router(),
    userRoutes = express.Router(),
    albumRoutes = express.Router();

  //auth routes
  apiRoutes.use('/auth', authRoutes);
  authRoutes.post('/register', AuthenticationController.register);
  authRoutes.post('/login', requireLogin, AuthenticationController.login);
  authRoutes.post('/registerAlbum', AuthenticationController.registerAlbum);

  //user routes
  apiRoutes.use('/user', userRoutes);
  userRoutes.get('/:userId', requireAuth, UserController.viewProfile);
  userRoutes.get('/allusers', UserController.getUsers);
  apiRoutes.use('/album', albumRoutes);
  albumRoutes.get('/allalbums', AlbumController.albumlist);
  
  apiRoutes.get('/protected', requireAuth, (req, res) => {
    res.send({ content: 'The protected test route is functional!' });
  });

  app.use('/api', apiRoutes);
};




